const { Model } = require('objection');
const Group = require('./group');
const Pet = require('./pet');

class User extends Model {
  static get tableName() {
    return 'user';
  }
  static get idColumn(){
    return 'id'
  }
  static get jsonSchema(){
    return {
      type: 'object',
      required: [ 'id', 'name', 'email' , 'active'],
      properties: {
          id: {type: 'integer'},
          name: {type: 'string'},
          email: {type: 'string'},
          joinTime:{format : 'date-time'},
          active:{type:'string'},
          last_login: {type: ['integer', 'null'], format: 'date-time'},
      }
  };
  }
  static get relationMappings(){
    return {
      group :{
        relation:Model.BelongsToOneRelation,
        modelClass: Group,
        join:{
          from : 'user.id',
          to : 'group.id'
        }
      }
    }

  }
}

module.exports = User;