const { Model } = require('objection')

class Group extends Model{
    static get tableName(){
        return 'group'
    }
    static get idColumn(){
        return 'id'
    }
    static get jsonSchema(){
        return {
            type:'object',
            required: [ 'id', 'name' , 'active'],
            properties: {
                id: {type: 'integer'},
                name: {type: 'string'},
                created_at:{format : 'date-time'},
                active:{type:'string'}
            }
        }
    }
}

module.exports = Group;