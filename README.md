# Node-backend


### ORM
[Objection.js](https://www.npmjs.com/package/objection) (2.2.17)
[knex](https://www.npmjs.com/package/knex) (0.95.11)
[knex_tutorial](https://itnext.io/express-knex-objection-painless-api-with-db-74512c484f0c)
[seed_migrate](https://gist.github.com/NigelEarle/70db130cc040cc2868555b29a0278261)

## Getting started

<!-- 1. express --pug --css sass // 建立之後記得進入該目錄輸入 npm install -->
2. npm install 
3. npm start

# Knex

## migrate
```
npx knex migrate:make user
npx knex migrate:make group
knex migrate:latest
```
## seed 
```
npx knex seed:make user
npx knex seed:make group
npx knex seed:run
```