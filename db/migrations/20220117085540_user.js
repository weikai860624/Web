exports.up = function(knex) {
    return knex.schema
        .createTable('user', function(table) {
            table.integer('id').primary()
            table.string('username', 30).notNullable()
            table.string('password', 128).notNullable()
            table.string('email', 255)
            table.boolean('active').defaultTo(true)
            table.boolean('is_superuser').defaultTo(false)
            table.timestamp('last_login', { useTz: true })
            table.timestamp('create_time', { useTz: true })
                .defaultTo(knex.fn.now())
                .notNullable()
        })
}

exports.down = function(knex) {
    return knex.schema
        .dropTable('user')
}