exports.up = function(knex) {
    return knex.schema
        .createTable('group', function(table) {
            table.integer('id').primary()
            table.string('name', 50).notNullable()
            table.boolean('active').defaultTo(true)
            table.json('profile')
        })
}

exports.down = function(knex) {
    return knex.schema
        .dropTable('group')
}