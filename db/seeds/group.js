
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('group').del()
    .then(function () {
      // Inserts seed entries
      return knex('group').insert([
        {id: 1, name: 'group1',active:true},
        {id: 2, name: 'group2',active:true},
        {id: 3, name: 'group3',active:true}
      ]);
    });
};
