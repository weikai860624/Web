
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('user').del()
  
    .then(function () {
      // Inserts seed entries
      return knex('user').insert([
        {id: 1, username: 'user1',email:'test1@mail',password:'1234',active:true},
        {id: 2, username: 'user2',email:'test1@mail',password:'1234',active:true},
        {id: 3, username: 'user0',email:'test1@mail',password:'1234',active:true}
      ]);
    })
};
