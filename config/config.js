const dotenv = require('dotenv')
dotenv.config()

module.exports = {
    pg: {
        client: 'pg',
        connection: {
            host: process.env.DB_HOST || '127.0.0.1',
            port: process.env.DB_PORT || '5432',
            user: process.env.DB_USER ,
            password: process.env.DB_PASSWORD ,
            database: process.env.DB_DB
        }
    },
    sqlite: {
        client: 'sqlite3',
        useNullAsDefault: true,
        connection: {
            filename: '../atolls.db'
        }
    },
    mssql:  {
        client: 'mssql',
        connection: {
            database: process.env.DB_DB,
            host: process.env.DB_HOST,
            server: process.env.DB_SERVER,
            options: {
                encrypt: false,
                trustedConnection: true
            }
        }
    }
}