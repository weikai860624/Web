var dotenv = require('dotenv');
dotenv.config()

const swaggerAutogen = require('swagger-autogen')();

const PORT = process.env.PORT
const outputFile = './swagger.json'; // 輸出的文件名稱
const endpointsFiles = ['./app.js']; // 要指向的 API，通常使用 Express 直接指向到 app.js 就可以
const doc = {
    host: `localhost:${PORT}`,
    schemes: ["https"]
}
swaggerAutogen(outputFile, endpointsFiles, doc); // swaggerAutogen 的方法