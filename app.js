var path = require('path');
var express = require('express');
var logger = require('morgan');
var createError = require('http-errors');
var cookieParser = require('cookie-parser');
var multer = require('multer')
const cors = require('cors');

var app = express();

app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

const swaggerUi = require('swagger-ui-express')
const swaggerFile = require('./swagger.json')
app.use('/api-doc', swaggerUi.serve, swaggerUi.setup(swaggerFile))

app.get('/', function(req, res) {
  res.json({ok:'OK'});
});

app.use('/api', multer().any(),require('./api/urls') );


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// swagger





// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.json({
    message: err.message,
    error: err
  });
});

module.exports = app;
