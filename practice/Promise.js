//https://wcc723.github.io/javascript/2017/12/29/javascript-proimse/

let minRunPromise = (someone) => {
    let ran = parseInt(Math.random() * 2)
    console.log(`${someone} 開始跑開始`)
    return new Promise((resolve, reject) => {
        if (ran){
            setTimeout(function(){
                resolve(`${someone} 跑三秒時間(fulfilled)`)
            },3000)
        } else {
            reject(new Error(`${someone} 跌倒失敗(rejected)`))
        }
    })
}

// minRunPromise('小明').then((data) => {
//     console.log(data)
// }).catch((err) => {
//     console.log(err)
// })

let runPromise = (someone, timer, success = true) => {
    console.log(`${someone} 開跑` )
    return new Promise((resolve,reject) =>{
        if (success){
            setTimeout(function(){
                resolve(`${someone} 跑 ${timer / 1000} 秒時間 (fulfilled)`)
            }, timer)
        } else {
            reject(new Error(`${someone} 跌倒失敗(rejected)`))
        }
    }
    )
}

// Promise.race( [runPromise('小明', 3000), runPromise('漂亮阿姨', 2500)])
//     .then( (data)  =>{
//         console.log('race', data);
//     }).catch(err =>{
//         console.log(err)
//     }
//     )

// Promise.all( [runPromise('小明', 3000), runPromise('漂亮阿姨', 2500)])
//     .then( (data)  =>{
//         console.log('race', data);
//     }).catch(err =>{
//         console.log(err)
//     }
//     )

runPromise('小明',3000).then(mingString => {
    console.log('ming::',mingString);
    return runPromise('', 2500)
}).then( (autieString) => {
    console.log("autie::",autieString)
    return runPromise('杰倫',2000)
}).then( (jayString) => {
    console.log("jay::",jayString)
})