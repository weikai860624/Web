const express = require('express')
const router = express.Router()

router.use('/v1/index', require('./v1/index'))
router.use('/v1/users', require('./v1/users'))

module.exports = router